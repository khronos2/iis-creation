# Ansible Playbook to Stand-up Windows Server with IIS

###### This playbook will run two different roles: ec2_instance_win and iis_install. You will need to have your index.html (or whichever default website format you choose) stored in the iis-install/files/ folder. The ec2 role assumes your keys are stored and being passed into the playbook. I currently import "aws_keys.yml" to access the aws access/secret key. If you want to do it this way, simply create an ansible-vault encrypted .yml file with the vars and values within. The role currently has a Windows Server 2012 R2 AMI configured within it.

###### When running the playbook (assuming the former paragraph regarding the keys is similar), you will need to run: `ansible-playbook --ask-vault-pass playbook.yml`
###### When using the ec2_win_acc role, you will need to change the vars. For the `vault_password_file` var, simply encrypt a file with a password in it using ansible-vault. Then, if you want to, encrypt the path for the password file using ansible-vault and put that vault string in the var, just how it is in the current file. Otherwise, just put the path in plaintext.

##### If you do not have pywinrm installed or have yet to do any setup for WinRM usage with Ansible, take time to make yourself familiar with it here: https://docs.ansible.com/ansible/latest/user_guide/windows_setup.html


#### __To-Do List(Feel free to submit merges):__
##### - [x] ~~Establish correct way to set timeout for WinRM connection rather than using a "pause" workaround.~~
  - The pause method currently appears to be the cleanest and easiest way to deal with waiting for the machine to boot up and have WinRM functioning. An error did appear which usually indicates an SSL certificate error but in this case (because we have the ignore_cert option set), it's failing to connect over HTTPS. Another method that I was thinking of was to allow it to fail, ignore that it was unreachable, clear that meta, and loop until it passes. Currently I'm thinking that it might be too messy but in terms of scalability, might be more feasible that trying to guess a pause.
##### - [X] ~~Establish a secure method to encrypt and store passwords that will be used to set the password on initialization.~~
  - Created method to leverage AWS default password and create a user account with a randomly generated password and encrypted with Ansible vault. Not the perfect or end solution, but something that is relatively secure for now.
##### - [X] ~~Create task to change default password. (Maybe randomly generated, set, and only displayed (Not saved anywhere))~~
  - Decided to keep the default password as it is already randomly generated and easier to recover within AWS. Opted to create a new account before kicking off any install tasks. Removed the password change within the "winrm" Powershell script fed to user-data.
##### - [ ] Change WinRM Transport from Basic to NTLM or something more secure.
##### - [ ] Add capability to specify git repo for pulling down website files.